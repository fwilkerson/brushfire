package main

import (
	"context"
	"log"
	"time"

	pb "gitlab.com/fwilkerson/brushfire/interactor"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
	userID  = "some-user-id"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewInteractorClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ticker := time.NewTicker(time.Millisecond * 500)
	for t := range ticker.C {
		cmd := &pb.Command{UserId: userID, CreatedAt: t.Unix()}
		evt, err := c.Invoke(ctx, cmd)
		if err != nil {
			log.Fatalf("did not invoke: %v", err)
		}
		log.Print(evt)
	}
}
