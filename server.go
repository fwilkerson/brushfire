package main

import (
	"context"
	"log"
	"net"
	"sync"

	pb "gitlab.com/fwilkerson/brushfire/interactor"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

type server struct {
	savedEvents []*pb.Event
	subscribers map[*pb.Subscription]chan *pb.Event
}

var nextID int32

func (s *server) Invoke(ctx context.Context, cmd *pb.Command) (*pb.Event, error) {

	// dummy code for creating a simple event
	nextID++
	evt := &pb.Event{EventId: nextID}

	// push the event to any matching subscribers
	for k, v := range s.subscribers {
		// TODO: Check if user or poll subscription

		// if subscription matches push the event onto
		// the subscribers event channel
		if cmd.UserId == k.GetUserId() {
			v <- evt
		}
	}

	// "persist" the event
	s.savedEvents = append(s.savedEvents, evt)

	// retun the event the calling client
	return evt, nil
}

func (s *server) Subscribe(sub *pb.Subscription, stream pb.Interactor_SubscribeServer) error {
	var wg sync.WaitGroup
	wg.Add(1)

	// create an event channel and add the subsciption
	ec := make(chan *pb.Event)
	s.subscribers[sub] = ec

	// check if client needs to be caught up on any old events
	go func() {
		if sub.LastEventId > -1 {
			for _, se := range s.savedEvents {
				if se.EventId > sub.LastEventId {
					stream.Send(se)
				}
			}
		}
		wg.Done()
	}()

	// iterate over this subscritptions channel
	for {
		e := <-ec
		// don't push events to the client until they are caught up
		wg.Wait()
		stream.Send(e)
	}
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterInteractorServer(s, &server{subscribers: make(map[*pb.Subscription]chan *pb.Event)})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
