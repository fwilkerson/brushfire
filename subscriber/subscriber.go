package main

import (
	"context"
	"io"
	"log"

	pb "gitlab.com/fwilkerson/brushfire/interactor"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
	userID  = "some-user-id"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewInteractorClient(conn)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	stream, err := c.Subscribe(ctx, &pb.Subscription{
		LastEventId:      0,
		SubscriptionType: &pb.Subscription_UserId{UserId: userID},
	})
	if err != nil {
		log.Fatalf("did not create subscription: %v", err)
	}
	for {
		evt, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.Subscribe(_) = _, %v", c, err)
		}
		log.Println(evt)
	}
}
